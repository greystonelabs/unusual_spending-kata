# frozen_string_literal: true

require './lib/unusual_spending'

require 'spec_helper.rb'
require 'securerandom'

RSpec.describe UnusualSpending do
  let(:user_id) { SecureRandom.uuid }

  it 'Returns state' do
    expect(described_class.new.trigger(user_id: user_id)).to be_truthy
  end
end
