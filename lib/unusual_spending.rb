# frozen_string_literal: true

# Main class for Kata, 
# as presented in https://github.com/testdouble/contributing-tests/wiki/Unusual-Spending-Kata

class UnusualSpending
  def trigger(user_id:); end
end
